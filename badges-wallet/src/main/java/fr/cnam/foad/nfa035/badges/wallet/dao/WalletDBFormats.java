package fr.cnam.foad.nfa035.badges.wallet.dao;


/**
 * Enumération définir les différents formats d'écriture en Base
 */
public enum WalletDBFormats {
    simple, directAccess, jsonBadge
}
